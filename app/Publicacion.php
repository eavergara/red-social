<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    use Notifiable;
    protected $table = "tbl_publicacion";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_usuario','descripcion', 'url_imagen', 'url_video', 'id_estado', 'id_resena', 'id_compartir' 
    ];

}
